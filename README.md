OH HELLO THERE

MY NAME IS WRAY AND I MADE A THINGY FOR YOUR FINGERS.

you probably like making lists and all that hoopla. Well boy howdy.. Let me tell you what. It's a reel riot over here. It's like a crazy list-o-rama machine up in here. It's basically the nutso-craziest-nut-covered-nutter-butter you'll ever eat. Except lists. Not nuts.

LISTS! NOT NUTS! A perfect motto.

I do not usually share my code on Github. Experimentation in a glass bubble!


HOW'S IT WORK, DAD?

Well, it's simple sun. You see, when your finger touches, uh... Well let me put it this way. You're gonna slide... Nevermind. You'll figure it out on your own eventually.


WHAT ABOUT THEMS PULL REQUESTS

Yeah right. Like you're gonna do that. I mean really. Really? You're not going to.. You are? You're being serious? Get outta town. ...Oh. Really? Oh. Well then I'll take that pretty seriously I guess.


BUT YOU HATE GITHUB

I never said that! Don't quote things I never said. I know you. You run around town in your big ol' ten gallon sombrero and gym shorts tellin' everybody I'm a smeg-head. "Derp." you said once. That's what I heard. How's that feel? Right in the feels it got you, I bet. Great. Write about it in your list. But I do like Bitbucket because it is free to keep secrets there. This is like the open-flavored version of what I do in secret. It's like the pickle surprise of Wray-code. One day it might feel out-dated. It might even be *retro*. But dag nabbit, it'll be something. It'll be... something.


WHAT IS A LIST, MOM?

"Look it up in the dictionary." -- every mother ever.


CAN I _____?

No....t yet.


WHY CAN'T I _____?

Because.


WHY NOT? / WHEN WILL IT _____?

Later.  I make all sorts of goofy things. Perfect examples live on my weird web site: http://rgbk.org


WHAT IS A README.md

Good question, Jimmy! And what's with that wacko .MD file extension?! Wowzers it's a head-scratcher. Hmmmmmm... I'll get back to ya'z on that one. Thanks for asking.