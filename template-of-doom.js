function Item(){
	this.data = {
		id: null
		,element: null
		,content: null
	};
	this.render = function(){
		var result = [];
		result.push('<div id="item_'+this.data.id+'" class="item">');
		result.push('<div class="editable" contenteditable>'+this.data.content+'</div>');
		result.push('<div class="drag-handle"></div>');
		result.push('</div>');
		return result.join('');
	};
	return this;
};

function List(){
	this.data = {
		id: null
		,element: null
		,items: []
	};
	this.render = function(){
		var result = [];
		result.push('<div class="list">');
		for(i=0; i<this.data.items.length; i++){
			var new_item = new Item();
			new_item.data = this.data.items[i];
			result.push(new_item.render());
		}
		result.push('</div>');
		return result.join('');
		console.log('rendering list');
	};
	return this;
};

var all_dat_data = new List();

all_dat_data.data = fake_data.lists[0];

var templote_target = document.getElementById('lists');
templote_target.innerHTML = all_dat_data.render();
//$('#lists').html( all_dat_data.render() );