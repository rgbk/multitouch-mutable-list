var myOffset = 0;
//var grabTarget = null;

$(document).ready(function(){

	$('.list-item .drag-handle').on({
		mousedown:function(e){
			grabTarget = $(this);
			myOffset = e.pageY - $(this).offset().top;
			console.log('grab start: ' + e.pageY + ',' + myOffset);
		}
		,touchstart:function(e){
			console.log('touch started');
			console.log(e.originalEvent);
			//grabTarget = $(this);
			myOffset = e.originalEvent.changedTouches[0].pageY - $(this).offset().top;
		}
		,touchmove:function(e){
			console.log(e.originalEvent);
			e.preventDefault();
			console.log( e.originalEvent.changedTouches[0].pageY );
			grabTarget.parents('.list-item').css('top', (e.originalEvent.changedTouches[0].pageY - myOffset) + 'px');
		}
		,touchend:function(e){
			console.log('touch end');
			console.log(e.originalEvent);
		}
	});

	$(document).on({
		mousemove:function(e){
			if(grabTarget){
				e.preventDefault();
				console.log('mouse move: ' + e.pageY + ',' + myOffset);
				offsetY = e.pageY;
				grabTarget.parents('.list-item').css('top', (e.pageY - myOffset) + 'px');
			}
		}
		,mouseup:function(e){
			console.log('mouse end');
			grabTarget = null;
		}
	});

	$('.list-item .editable').on({
		focus:function(e){
			console.log('focused');
		}
		,blur:function(e){
			console.log('blurred');
		}
		,keyup:function(e){
			console.log('keyCode ' + e.keyCode);
		}
	});
});